'use strict';
const MANIFEST = 'flutter-app-manifest';
const TEMP = 'flutter-temp-cache';
const CACHE_NAME = 'flutter-app-cache';

const RESOURCES = {"flutter_bootstrap.js": "763f2bdaf20755494aa0a1c8a3a14d2c",
"version.json": "5f943c32b9908eef03c55408d799eaae",
"index.html": "1c972708363d2dad3dd88d1557208509",
"/": "1c972708363d2dad3dd88d1557208509",
"firebase-messaging-sw.js": "34a8bb1aafe8fc3d2e4343b4ea6cca82",
"main.dart.js": "5305d2f160f14e43179c31eb1810bbd1",
"flutter.js": "f393d3c16b631f36852323de8e583132",
"README.md": "8f22f2e4e504efb010c9fd5bb3443ecc",
"favicon.png": "c5c64bea46d69271cb4b7ac50a6d3b4b",
"icons/Icon-192.png": "ac9a721a12bbc803b44f645561ecb1e1",
"icons/Icon-maskable-192.png": "c457ef57daa1d16f64b27b786ec2ea3c",
"icons/Icon-maskable-512.png": "301a7604d45b3e739efc881eb04896ea",
"icons/Icon-512.png": "96e752610906ba2a93c65f8abe1645f1",
"manifest.json": "c52654258efb7c33879ff394e8da3fe3",
".git/ORIG_HEAD": "44f89327b86a65699372cb36d1d17cfd",
".git/config": "ffdd965acfb19fe4b723044de8c5ab65",
".git/smartgit.config": "075a95783a20ba34433c20b847f296b3",
".git/objects/61/e28b78fa86976cf09c5976443a4a6ccda171a2": "c3ef0106b0db26604a4c173466c86e53",
".git/objects/0d/213a66321a3d4445313d9dcfaec943f53d2ba0": "90813963f45f2578102d55b10edefca9",
".git/objects/0d/c56edde812a4fce4c5bf23efd72c608619ae89": "28885f3cfd59c01e8231478c46367c3c",
".git/objects/95/965ab119bf37fbd586d6628fa5191006b4d0a4": "6fd443d34d50ecf25916822734916c4e",
".git/objects/59/3cf0b31ef6e0b40592d9edbd588382a26e99e5": "18f95dcf1848ededb46d37966b50c3e6",
".git/objects/59/6e1ce34ed438ab5cc99c7fd394c76703b8e373": "52cc5299c901121e69d2998159b5ee3b",
".git/objects/0c/250c70f47b96f998b9ccb1f1bf04c3ff6facbe": "38c287300fb6e1c30e9a8b684a463175",
".git/objects/66/2aad4e8b535f423e6e991956de85d9ab6e1139": "8dbe9e6a2fa2f2a9db4b6ee2aee08c23",
".git/objects/50/eb497e9566067cdac76407fe83633266b8b78c": "0058251665bc9bcead575d714bfef6e2",
".git/objects/50/35799419dbb70c1876d2544c39d49b0bc1f7c2": "de9191fa1c6cde2b395e187efbb72842",
".git/objects/68/e85e90e873046f5edaecb4f648cfefd0d9b5da": "a022bedd5d7016e3319b471205f76484",
".git/objects/68/5f072098ea9c1f5bb622875441deb6cf3ad6a6": "0ab0c5366a84995e48824ba62557e531",
".git/objects/57/1ad75ce24b22c90e8c3cd1a4e98613b5aeb2ac": "29535beafe9004f9ba65d8d375ad3388",
".git/objects/57/5af994335a2ac5aaeed8fc554c46392e1e7c81": "1ee699163a764470908a9d6882e898c3",
".git/objects/3b/aebf5dbf4dba4b581fd61f633ffbd5dc7024bb": "176f89c585631345f6193453e128b047",
".git/objects/9e/f1e2538e70383d308a38cad81e313a6f8cb1f5": "02e9ce7984b0b7fa0df499e4a3aaf807",
".git/objects/9e/17800717b97ff7533ad13394518b307044c00a": "7498d6bf4f1dffd96b29bef5d95fbbc1",
".git/objects/6a/a2768a360e2a94cc821a9a3327c65a0bf78385": "104c794b89003857afe45f9f48842e02",
".git/objects/32/e35a1b7fc6cbd45c309a396c8669faafe32a04": "a72ffe4964fe440aec76729cbc221192",
".git/objects/32/67acdd01ed70a681cc9758c5499e27a4d3a209": "9da69f0a30d755d27743477a942a8f75",
".git/objects/69/70ef9a12debf497a07e6a1edd7ec127e16bf2b": "422e906eee886dbd96ad55181a66afd7",
".git/objects/3c/677c4a5c190c963e7607930edddc666b4b51bc": "ccd48053950fce0b01fefbbb10abecd8",
".git/objects/3c/fbec14735803c57cb646521929e9ef5e52f02e": "ae6834d6df35351c5273a1db48539e44",
".git/objects/56/e02efab2eb5b0f5ef98234fa1c920f8bcf4afd": "d1556700f15387e9f2c23909eadec775",
".git/objects/67/f0fdf79ed7eb59c5db09aa4173952bafddcdd4": "70a51fe58a46157357058dee065e4ac4",
".git/objects/67/839627a73cab664e1b1b9e60835fc5518e0bb2": "4c4045c82fc54a656ba8b1d561663ef4",
".git/objects/0b/7d6739baf3f9828fcacbe9d04eeeb1e1a2e78b": "6340a5d6b96b66254b52bdf1fb1bbeb7",
".git/objects/93/2dfbbf35829e5a6f5525faef9756c6c7aabd9d": "f417dc2ba11adbc943b4e5a8a98d2e72",
".git/objects/0e/eedc5cba8c6ddc84adfabb915ba62dcfd16d9f": "1e2f45bd9503fbcf3f54bfe56c38dc63",
".git/objects/34/10c6ff055eedcd33816c5710a0c6565d2ab719": "6080614a7da4eb0a4063c62e1855077e",
".git/objects/5f/93423a5780361446af4ca8d4b283b928aa2d50": "7785292a4f7f53cbd29ba8156dc1e65b",
".git/objects/33/8ff2157e397fb9e208465d0c2aafeacd2e41e3": "171c5cb5a2497e1dae199d7be4812e7d",
".git/objects/9c/ef01ddb913338680c127d3eee426b234b7322b": "d474260bf166c70102c466ff28195b58",
".git/objects/a3/ff58fb7f48f362c28af2fc929e951e5bb206b8": "056014d9da0db702b11fe479c5dce75d",
".git/objects/b5/985600f9700a1bdd3faff22e1b6dbdfe472df2": "aca791db73126fcc932c2231eb2931ea",
".git/objects/b5/1aea92599afe491825eb40f62c25eea030309b": "398825516ab30598ce1c4d7d45d7447c",
".git/objects/b5/ac4f0ed4951051e3b83e1b7e975e3a3a1a0f18": "13be376d1b6ff4b3bdf81912293def0b",
".git/objects/d9/d5d38c9503d608cb5f9fe7445983382f3e04e1": "61181f1d0383fc0048733cfc08b131e4",
".git/objects/ac/f50cb0944b0627f3d8b518d28d916af63b0491": "c16f10ab074571df7ad0be43b74bdd21",
".git/objects/ad/b46a881c33da2bdf5979aad386550b3aff146d": "58baf150426c5812b3fd79e46dc4f3ea",
".git/objects/ad/6f1b3adec61f3aee04fd5c2bac86eb96994cf8": "589415405da1cb623f322044ead9e870",
".git/objects/ad/9582f371cfc6916f0e76103d4ad2c251f76359": "2219b888c8042757b428dec29ad9cdc5",
".git/objects/bb/a76c24ef808cd2b051d04858435b1d2bf1d620": "6addb93325c8433acfac8b7b6fdefec6",
".git/objects/bb/a84866a63ed7f8fbb1d082b0542e98e105a669": "32b52f366bd38bcbe2aa85bbe9a02b18",
".git/objects/d7/4ffb4cf16b57adf012c4b9717cb231d6ee3209": "d954afe7f0430248c99e62354bd7d48a",
".git/objects/be/06dbeaacd6aa1cae40b044b765f4b04e5b7a9d": "3863d60827ae12fd26afb41e0e57e5e8",
".git/objects/b3/b031e9a22b8e319673109b72c0c0e7a1b7b434": "a7df1945846b4633a6060b1c0a58b0b1",
".git/objects/b3/2f4ab96d0ad93590832060507c8da34813839f": "71d7975a85b7c9e1073cd3418101446a",
".git/objects/df/d7a6c5c38f8baad2be9d59e52ce3bbb6b97b35": "c8d1a6e80d29b254e6b77147761cf3ee",
".git/objects/df/98155b228ebd97f7c053b2f6c07c3a97767c2a": "8e16c14b6f2868edf974e1c6c81e703e",
".git/objects/da/221c2a927fdf1d95d4add6e707b2d6c1a5c642": "a7e78ef6babbcdad849655e7f5e6d4e0",
".git/objects/b4/7200b4444ae621b67ecbaa46b8abe8473c55dd": "a0a65ca4b894229e4a4260f237ab1533",
".git/objects/b4/d42610eb6a5a1613aaa69f99da8fc24aa97b47": "fc0912a80c0a9cc57c69adea5a227cb5",
".git/objects/a2/6b1fcb447b2980ef874ee29fa70ef3ae0dffad": "6f8682b94fdd5dabfddfc19cd43aba43",
".git/objects/a5/22e287eff135c0802f7d4833d7c04c6a27a79a": "ae057cbea4873f2c2259384d9d95e663",
".git/objects/bd/02e242fe160ef63087f44d4c6037e1e9f58b1e": "05de3c4f31871e7444ead06ec9f10aab",
".git/objects/d1/c88ef7911684c6b3c07d6f8f234f48caaac1f2": "275f379538f66ecbf70eec567225f0dd",
".git/objects/d1/15078175c1237dffcf505e639023f63affa920": "1402bb5c11ae8e9050ce2206fa4d66c9",
".git/objects/bc/fc557949dbaf5bb42ac54b98db26b7aac7df7e": "2b3112dab7a8c1106d6edf4b179c2280",
".git/objects/bc/18b6fd38b91bf1c5746e9de7e9f4ce21715494": "e1a8015d71831feba60e4a520639a353",
".git/objects/d8/c732a408c450323fe1387a7fd15a8739b4f92a": "b599ff63c6971e1587d0124d13cabb4e",
".git/objects/ab/7ddc76dd16b990b012ed4166bdff4c8a453e9c": "d1a6f9cd52445c40c007752087efa29f",
".git/objects/e5/9d7b869fb564cff2748addc3eef1ae73072298": "8bd679a72639155932bccc1ad8c85185",
".git/objects/e2/2160fe2fc17e66ef01d9ce1976e685e816f351": "7cf3d09e19dc2d8005ba7047f91d9994",
".git/objects/c7/c66405f1d0dc9db844b7dc06a2249a3d3e32a1": "70d2865162fe9399c2e68c2a13b5a988",
".git/objects/c0/ac88d87f4ba5469872f4282fe1807bb7c544e4": "05190a408659a6c4064688577c2bdbcf",
".git/objects/c0/0d07b14aaccbcc70296a2ff297acdeb95d6362": "2ee9c156efa5e11a63bd02d245b2095d",
".git/objects/c9/164060a13387cd0de38663336e886cba2809d7": "27476aa34f1f520c1a3b2553b6e07282",
".git/objects/c9/062b15c65b16c0f9b4a18e7efd739f17e2dd7e": "25173ba68196c2583a99a14caa26dd57",
".git/objects/fc/7cfc79016f34bfd202b1807bcaf590f7b4032a": "6a486606106d219bad6f673fc516d0c5",
".git/objects/fc/7e4748402504862c9b929ca2cba1e5ff307273": "7e7abb5a131640b593a0595c376647b2",
".git/objects/f2/dee837d9211a9808527dab02fd493adb4c0f46": "ad7fe524bfe8230e0fc2db3fcaacc46d",
".git/objects/f5/a962aa143246b48767e81ae3f55ecc9355f3fc": "672b9de61eb18b78fb1670bab2f4c343",
".git/objects/e3/ea0ded02ff4333a847b2932775b8cb7ce6c617": "b00730347e89ee5c2da4fce7958b0649",
".git/objects/e3/36b56dc846c539508d3663f7d44b1b26117307": "1b757340548705e7c2b6acba17f1c948",
".git/objects/cf/d6b8f086d5ca65f354cc29076dc4e53ce934b3": "bb341c2fb0f2b4764bb55501958499b3",
".git/objects/cf/402fd8f8826fa47984f6b79d31e9efa12fcb1b": "81d3ce497540aa01f33fdf5f23020665",
".git/objects/fe/c3536b96fd9e2e592d28eeee26ddd25a29703d": "f4589b6b3e0d0b67684492842062900f",
".git/objects/c1/6e141b50b0c057cd30ad5c445c7bed778d0eac": "dab6f6e6b57e89fd646e312f44511387",
".git/objects/ec/82ef82ff04735f754cb8d4b7f770ae2844c914": "699841bd1ea46f68e57cca198b7da3bc",
".git/objects/4e/738c4912077baa4cf35b5865500d3dbd3f0085": "24fd0deea23daeea888e6afc32aad4b9",
".git/objects/4e/286f8e885990eed0b055eedfb45681d21f1eb7": "3c38c963020a2d1d13071301de42373c",
".git/objects/20/0939bdf9550c7b1521d5e728d6aa838d91c829": "e967aa20fca8ac5068288f8d100b650f",
".git/objects/18/f6aea821c5eca5cd7e3cd15553db9c8934d94c": "4c8e5d7ef5dd31a6f92aea70870d719f",
".git/objects/pack/pack-86f7f909029f72483989d077a212fb10a7e8df56.pack": "e404adce44b7d52851f4aee0dd55e5ed",
".git/objects/pack/pack-c1cdc315a7fa816ddfa3d5f7d8343f58a3501ab8.idx": "7f1f97f4bdba135f7fb665a33f6ab086",
".git/objects/pack/pack-86f7f909029f72483989d077a212fb10a7e8df56.idx": "bd1d85716a678f8cfbaf349eb53450a5",
".git/objects/pack/pack-c1cdc315a7fa816ddfa3d5f7d8343f58a3501ab8.pack": "4af4faa729822e4d545b34109d532027",
".git/objects/7d/5c11b19a10db43adc8f7be27fcce8a716c3ba4": "305ba2f3d46492d8d5cd36881110125f",
".git/objects/7d/10ae89767a01cec586273c74fb091dfd6ba2aa": "841085a8b76f47c7094182bf711149b2",
".git/objects/29/a27806db20b109820c08465a3cc2197b37f05a": "71fa260136cc7d36f5ddd9bb4dadd04d",
".git/objects/29/7c56d73a7a76789f893c2c686e4bc04cfb8cf7": "c1faac12f0bcab87ae9d3e6903c96516",
".git/objects/29/8490b471b9088c519c864d1972389d9afa8df0": "4984f814c3188aa56e37c0200f51ad66",
".git/objects/7c/9e25451b62083e2fd815df7bee444acbd746ad": "8fbf05c1ebac6fd080f69894e0f0eb47",
".git/objects/7c/4b6f241913333423b1dc1d519892e528a7827b": "472cbd7f4d8a80215c7aa0f3f3cb5ffc",
".git/objects/7c/5f06d562b3e5e0bcc67bfe7bde418e6bec6290": "6d334a2ab0d85f393083106dc9ab83de",
".git/objects/89/d0d8d4cae2c2afdceac06f00f8bb646791b297": "165179b50caeed2ebf4ad6c345346828",
".git/objects/1a/60266e1bc65d133653c05ba20e6f562544be86": "ba819d5c51034a8cb9e8c380423e1ea5",
".git/objects/8f/c0e7773c90a9c7e8985e408e95e43b794ab44c": "954382e4aebec406ba79bdfe64be93b2",
".git/objects/8f/1492740e1201c36e5835b9540b4aa0b2214f17": "17180cee9781b4b34533f05761dcd418",
".git/objects/8a/cd414c8c138f08961aa361d706b66fdc481b04": "3795d4d8e09586157b11177e440e9472",
".git/objects/7e/3c5de1d5ca5dc8c5ffea26b4823fd14ee7ebb0": "5592f11d9d99750af64ec17375330db9",
".git/objects/10/480c56896a11ff8c9299d6e6fc6c834fc6c4d9": "4dc6bddddbb3a15436d9d060008fade3",
".git/objects/19/ffbbd8662ce6bdd2fefe7a1aa34bb8d3628d35": "aefdc38190ece1fba8709a851f968e06",
".git/objects/4c/a64956d5e19df96ce6bb161eb1c158cceb6dfe": "101eccb666d33e2af5451f47891df80b",
".git/objects/21/4545e0857ffad15a5998084f7069f231c1dc22": "508ec50e78afc5c2d604127451ea0b50",
".git/objects/21/c2ab39d1c88eb4f397c3092b0e43e20964b4a7": "576a1da88c26b9ed3d7288cb865a5030",
".git/objects/75/0a8ca5648f111599876d99274718146c1dff32": "2cbbd7bc3be3b8788245a6d6c2514ba4",
".git/objects/86/be61bc4b5d73e01e5aeeb4608486a26ef64671": "7296da29d665b72e44c171fe99a14275",
".git/objects/72/df11ba78a08bb16a7337c23c46453edde5bf71": "08c7a96d138f4ed9c4dfe4d5ef0c9d2a",
".git/objects/72/eb06350a4d6ebe048c6b26a52ad8b52c815408": "c6676ced4644e69e66506530f5d6cac1",
".git/objects/72/1e92d1b986db9faaefbcd71560c0dfa7d86307": "b8af19875b3b1b6cf22dd356c1d007dc",
".git/objects/44/efbf63fc8dc754cbd49314e86c97da98dfcbbb": "de1542fcf77b2ff2184bd9d5c1588e70",
".git/objects/44/e0e63a9ba329127a8b28e3dfe8700a3b7cacab": "4dee822736d398f158dd69afc19b9338",
".git/objects/2a/0e7b76ec0678cc7bf58894180987b474bf14e6": "302e1d06bf8f5302e250eac755b148cd",
".git/objects/88/2f7f63fc5a24200639cddeb13cd9f717d563f8": "64fd30782041d5e4edf511a0dc3c693b",
".git/objects/6b/85ee632cd3975a16279209b1f6f5ad97c39d31": "7b32748a8795dfee608cf071806b40fa",
".git/objects/6b/2a9a9de6bc6d9b2cae11ec4d6f03e0c1606a22": "d624207eceeb2562228a1c0d61be229c",
".git/objects/07/bce25844c935446c94a42b684a9a577d64b6ed": "fcad9c4945dd2886c7e3b0093f689db6",
".git/objects/00/cccbdf193198b0c7c7ca5e4d3e86ae8f4e9129": "cba02b4a7777df39b5e1b42452ae0800",
".git/objects/9a/4e5898c69551ac78346cb045f432dd3bedc1a9": "0270265d10dfa65d1485b93ef1595bd8",
".git/objects/36/8ece360022e718dae0ff50173aad322c011f38": "b0bd7160e5818448921e45ed84dc4bc9",
".git/objects/09/d532ed1622f26a35a4e7de27ae6ccf3479e784": "41dbfb23dc50677fba600aff26313027",
".git/objects/31/dfc3dacc3fcefdc6bf7db14195e12635ecd033": "c3f4989cc79fd4e9f9b22dd850f70b3d",
".git/objects/65/81492eb9d2f558cbb175f073f21eed776d17ff": "2908135317b3671079c5dc54d7f195f8",
".git/objects/65/77b6db3473eb534bd27ff4e377694e1e55e40a": "a5d77623fe4c6484388f8fcf696a1b49",
".git/objects/96/c7ff7ada8f668fa7f76a3e43237d3b5e141f9a": "2f86a46745bfbb69bed8d617ecb36ced",
".git/objects/98/9a26786ec17015f7464b46efccef2c18e9e684": "b1e9f8f635648c1c4064fb9dfce7a725",
".git/objects/53/055e0f742d31fe9c3525eb7fc51bc8dfb4fbd7": "ae7d0856ca502dc4c91dc4f8962cfd48",
".git/objects/3f/e279c1c6d0f75c046009244dc0f81700938362": "1f4661fb16f4fd9d57ea4f00edfbbbc4",
".git/objects/3f/6e35b41ce5ea2fe7e1c049753e1c768ce9e33b": "a219280280cb09275ca3a1f63912669c",
".git/objects/3f/bf4786c7d5ed9a672a564f794047fb5464d042": "c53344af0b7867c1c46c8f50bd3192c8",
".git/objects/5e/8c8c40d303f385e3d755954607310d7c06fbab": "f70ef5998b44d4de276f3197f3d68b83",
".git/objects/5e/99a2b5542c9d484ef37edffccf298883164e97": "535e523e3ed857b66d9f78c6961a0fbf",
".git/objects/01/70b53297b54969c9b10261ce33caadf2f4c97e": "ecf4d5a44f6fda460016a0ddf25f0188",
".git/objects/06/affec8eba519437cf1aa350b3900118bcf5644": "18a25ab855e52734854010f2025d811b",
".git/objects/99/f09852674024d04f6fa6543d5a1c2c9063f5b6": "04f80173aa9d998d394b82d8dc198c1c",
".git/objects/99/5a6e94fd9c802ba078e4cac481282afb3732b0": "ef2e2edeed6dd9b30dd15546181af0b1",
".git/objects/99/2d0abff9b7ab17065ea74e236b79a11472a83d": "42048b36b927fd01b5495a82a45c9d3a",
".git/objects/55/c2fd35c85fbff6517deb1a1302c9a550e75e42": "c70aa8b7e5ddcf8d999c6a307f80eac0",
".git/objects/55/ff320336ebc38f5f843b18c221624a40f362b1": "4762172833593f79401febb4362d7ee1",
".git/objects/97/747998754fc366388474522e088391a9cf7e69": "563954cde7d421c0825a0fc63a860b4b",
".git/objects/97/50ce7131cca1094a122c84275826ec79f275d7": "68cae0ae329f343e4701a111f2079fd5",
".git/objects/63/3c5542b1a696bb94ab146c97afefd47fc47ba2": "3e52a92bb5a53b953c163d04d410bc97",
".git/objects/64/5d6ae0468271a36713a305a048c0f713c5181d": "5ca7037ce43325e1addb0e47903c00cf",
".git/objects/64/0dc6c2bd720e0bfa8c6e7a9de8b20814e456cb": "32f7d7ef8eeb394c7144dd342abc086e",
".git/objects/90/d9d7126804f48981fb6bf3e6e94ab4b71c2227": "1f24ab0bf82f177353ef12b8e294614b",
".git/objects/bf/855cc5995c421e48490213d14d744a11c0de40": "75f41f9cabe677a7524ec2c0751c5ba2",
".git/objects/d4/ede03657bc37258808c6c16f046d9cbe78d804": "52065af383c420ffb43b9b8ba16a98be",
".git/objects/ba/e152799672fde03869821fcbb3109591f7c784": "9ebf65fa918d9b23c7dcd1ccef1cb4d2",
".git/objects/b8/0944542f9e8b3fcda9ad3d6a962e2c6bdec1d6": "4f6a9db8ab36ddda48be7dd9e86e9a0b",
".git/objects/b1/ed283b4c0d6aad5c80ddcef9247dfb0b580a71": "630d4e68cae845580f380664dc80f82f",
".git/objects/b6/7692d657a21ca4dd45d94e2fdf3e90114b74f2": "080c31dc3d0c93530db7aa7607c0d329",
".git/objects/a9/2656bf261c4142bdeb1be2c583d47ab34011cc": "f74c05797c6c480ad19f3dc5e8c8c836",
".git/objects/a9/773eeac706812ba78b2c38c26bb04cacdb9c8b": "2e83d9aad17cfc73cb31c07823c6d6a2",
".git/objects/d5/8436ed8deb7c77c13a0d224683a4a1ae13c109": "e99c7e8c77e40b194d9490089689b31b",
".git/objects/d2/f8390e9f94c22b1be5e40fceebb1f6e938019e": "3333917aa1505e4c60ef854d558e1919",
".git/objects/aa/7fbe2549b9a90d2ad28003210d8d615e9ffd05": "8c3c1cc542a8004025f14006f1835781",
".git/objects/af/5f23eddeded585c9991921cfa024c11303a489": "9cb576e56d5766d4be8db66103262a59",
".git/objects/af/526312ea2686bda6179c2199275a12b062ab1a": "c98423612c974ccffb7e9bcc0a7dc879",
".git/objects/af/06f6462f331bda8c5dd89ac1bcdab2025d05e6": "5621322f1599f44e2733ab3e3e607860",
".git/objects/b7/cd4a21f5876c028a0fd74de22f4e670accca2e": "a7485651cca550cf4480fe6274f35147",
".git/objects/b9/036132726d1cd5530d44d87f1299810ab3cf60": "42e78f97654cb37acdabbe928c72cdaa",
".git/objects/a1/d5c830e0ed0c3d7c85e75d600feb52994b63d3": "61dccbaa5829006bab6b1203cef747b4",
".git/objects/a1/e6a9dcdb5d7c5c079a4f2ab19465b44714c185": "c6564e72ed7038784e663f8959270b47",
".git/objects/ef/9f6520457602fb5add4448066dc1f5065ef102": "4ef877bf26ac85c6db8aaaabfe001a9d",
".git/objects/ef/633d99b237897ece4386610beeb35bb3cb860c": "eaace967d667782f06b873a5b89332eb",
".git/objects/c4/7db15d5f730601a026c187759453107dd75d33": "78870b9dd3306a82b7c8f15aaed07dd0",
".git/objects/c4/2da6cade2316495c9b57f3405586593cc1f6d5": "8e4b7f54b8d47bbca17008c7fbc010bc",
".git/objects/cd/d50470260b666cd94b58b11b030ab8793b3afc": "70e7a987a90225ca15c46d4e72fa55e4",
".git/objects/cc/284ab6fad7c237a46ba1d648594708e11829d9": "6ec928c5cbb217141d30f37d56decd31",
".git/objects/cc/83d39d8beeb1eae70dcdd57a13f8dbd8f2d936": "96b9df55d0b5f9521f1f11214c661a70",
".git/objects/cc/aa8bf69ce2f3ca8399398a7386ee5122b8a548": "4170b344092d2d71132f7e50961a2f32",
".git/objects/f0/6eb65b32277e1583c7929901de7c39fc80e983": "c9745d99c2851554aa9e6271fa6c16e0",
".git/objects/e9/3b3baf89ca1322cf3f9a90b6d89137e48ab33d": "d36bfe2563a110c6d316b7d106a982ab",
".git/objects/e7/efea19ed39ff2a9f11faae7dcfab50c4fd3771": "53229828068ee8eb7634dc6673a4d961",
".git/objects/e7/4216d56234daec87321d8a30ef1bb5385fe28b": "05c14ccd8a4921108a54919a3f153c05",
".git/objects/cb/9799d25dcd067b44dd7852e66b17c883499811": "02f638d3d85d058080336a5df494dccc",
".git/objects/f8/80a7d0af1194be02e41ba0242f7e466e54281c": "93a1c0f1b87e1d7a8ad6a99c70252ddf",
".git/objects/f8/0a47f26a605c8e6f0a5a555d92dc2b7b0c771f": "1dc50908cebabc7823876fda6d09f549",
".git/objects/f8/bacfdc7010aa72301c2689ce702b6c337a832a": "55f845f5c417ced414ac704f339c44ff",
".git/objects/f8/7c23f90e86f90544960c3000d5baf7807bf946": "44e9168f8f15a49390553640bad3b52e",
".git/objects/f8/01b8ba6311b19b141c091ba1f1cd8ac10c6cb5": "f347a3dec75f79885b6c3ce4dc6ca980",
".git/objects/e0/bba56357f98179060d11b40681de133b18d84e": "50b551c53ca7dfe8b8483594aa9269dc",
".git/objects/e0/b17e03b613108b641822bfd882a32e32b91705": "da0da21b4e9af557e43fbd231178b6ea",
".git/objects/2c/6307eda089d744161628549b9ac1898f4ae050": "598c0cee4d9463801fefbe75866cd44f",
".git/objects/79/c87f07dec27fb4e667525d3314cc2200e63e22": "b3b3cd8b18f13fd5ff12a16531572895",
".git/objects/1b/0fcc3359b777995d44fb857841da6bbd81a763": "011b532d4e5de7fcf242e1910b4de0df",
".git/objects/1b/9b2ba6daa696ed5ff596955d3f931e346677bc": "e5d66f99952325a1307eb16ef469488c",
".git/objects/1b/07abcae3eb9b04edb428b5a0c154d7dd4b8605": "c9c6d7392c2c0ce8933e08fb17411892",
".git/objects/1b/bf1d52d29909fcdcd06206e223589fe8e3de95": "2512b3fc07b746399faf4b01b68f56d6",
".git/objects/77/a8749c611f11ad6aaeb938e0011c77717df131": "ee1778344e0c9604c4180503c6486fd4",
".git/objects/48/b1c776c9ef541a50b44028a745e5881bd2bdcb": "22756db9bbfafbbb27f3fd80e31f9481",
".git/objects/70/eb9f8c0a83f510592938cc2cfdb3b554ebc657": "0f03116f90d3325ed853e1b3fd15a9e9",
".git/objects/23/33583c04273652f913d758558d42978c260b86": "b3e718cce3ff509c2ba07b0004ffa66c",
".git/objects/8d/e34ff6946de77efeb0a7726fd06b91c2be79aa": "4309031c1a42242cb513bece8db86375",
".git/objects/15/94d4653347473fc0e6790367d85f48b99cf618": "23d682738581b7a4897458e7da99a0bd",
".git/objects/15/b4050f74e190e9a5f04bed83d821e715e19e92": "3f6df142999f62d4461a645fdd9952fd",
".git/objects/15/77f9be358dabe18294af7dddd0f3c935b62ecf": "e26027c5ad54cdec301ddb82a62f5a48",
".git/objects/15/01c4d8b4dbad320d322485768545d6c5593bb1": "557becff9f94d0f48a4f3f42db368d84",
".git/objects/15/76e2d1479ed19b2a7fda7231d96457aea9462d": "a9067d5ffe235a0c5c1418f828ca8d51",
".git/objects/71/f190188ffbdf117bd50c90abba8c7d8a04685f": "e29722eea1186d2436acd6e68d13cee8",
".git/objects/71/f01b46c2cd2c85206278547e87dbe7d64d28b9": "bdd6efdf763f654fe3279e393cc43f03",
".git/objects/76/395e43ae26c14132e492f1fc10b831ef7dcd08": "5253b6a2a848ffb99e74424d48ca6ba7",
".git/objects/76/15f8ce7e3bc1f52d88ef819b8b8e12ee43b082": "9c675342a807b944d6a5da2086fac998",
".git/objects/1c/2975ee358e2489aaa218e7bb4fac5fd8a91ef6": "6df729b3584c5b092a7de1bdca674674",
".git/objects/1c/afdbfde8c7ffaaf7ecf15d31a9dd21b0f45855": "609394c7ed9068c13f2dc0860a88247b",
".git/objects/1c/c49cf6495b6259542e37536b4e6f272eb2af2b": "920912099a433e2ba0465884e731b1e8",
".git/objects/82/691d43a212ceff484b83d7ac4765cfe3bc659a": "c1999f44edd328adc93ce8b682ef10cc",
".git/objects/40/d7f6ced065019669a413afe8f07e73e9fc9589": "49fbdd61279599977142023123ecf2e7",
".git/objects/40/2cd4af20e61d42bf5ef0715e041518824ab4f2": "8dfaa23a0f2b6ff0ab03078ed7281bb8",
".git/objects/47/65db8f78604eb585e66781cd3bd13abe62b2b9": "679376e735568a0424b07f190e9ade13",
".git/objects/47/77e37fcd1c359288a02ca4183adeda9c8507e1": "dbc1870d9ecd730030c82bc7265c456c",
".git/objects/47/eac3ceaf5bd54616903ad9ae86bf1bdd201368": "d88563a83a1da88951808f15f8ca3fef",
".git/objects/8b/d0eb1f08cf526af224d4b79b359aa160f9741e": "1173987f4fe9b2211087470d48e9127d",
".git/objects/7a/e292015e47c26420d06f1541d4b881ecd5d33b": "783441f681867963e9ef229c3212e5b2",
".git/objects/14/fbd7317de549436ec87512bbe28dfbb4399a1d": "997eb4c00f3b96332171c8f45b658746",
".git/objects/14/ecd630bbeefb2ecf973a6fe97d51feca5ff140": "4b022bfba797dada85abe318723891fd",
".git/objects/8e/830fafe7bf2e628f3f79013c1cf7183f1f144f": "5530b7f0eb6bef66cb1afa1ddacee5d0",
".git/objects/8e/b94099ec2ce8685ff67edbd32304372e098cb1": "1c20efa8d5c2062647918af380dbb9bd",
".git/HEAD": "cf7dd3ce51958c5f13fece957cc417fb",
".git/info/exclude": "036208b4a1ab4a235d75c181e685e5a3",
".git/logs/HEAD": "69540410fb855d95ee804cbfb3cbcc87",
".git/logs/refs/heads/hoa/form_booking": "934b430bfa7067c3f0bbe60627487e36",
".git/logs/refs/heads/dev": "42defcf03296706a90c04e4758946291",
".git/logs/refs/heads/main": "6504b075d04033c6859c6192c7c80025",
".git/logs/refs/remotes/origin/HEAD": "c69423b9b12b1ca671f77a96b0bd3ca7",
".git/logs/refs/remotes/origin/hoa/form_booking": "51d7eb6f4c8ad720c59965a3bf07ba8a",
".git/logs/refs/remotes/origin/main": "7c0896f37b787ade3dbeebb4ff2675f4",
".git/description": "a0a7c3fff21f2aea3cfa1d0316dd816c",
".git/hooks/commit-msg.sample": "579a3c1e12a1e74a98169175fb913012",
".git/hooks/pre-rebase.sample": "56e45f2bcbc8226d2b4200f7c46371bf",
".git/hooks/pre-commit.sample": "305eadbbcd6f6d2567e033ad12aabbc4",
".git/hooks/applypatch-msg.sample": "ce562e08d8098926a3862fc6e7905199",
".git/hooks/fsmonitor-watchman.sample": "a0b2633a2c8e97501610bd3f73da66fc",
".git/hooks/pre-receive.sample": "2ad18ec82c20af7b5926ed9cea6aeedd",
".git/hooks/prepare-commit-msg.sample": "2b5c047bdb474555e1787db32b2d2fc5",
".git/hooks/post-update.sample": "2b7ea5cee3c49ff53d41e00785eb974c",
".git/hooks/pre-merge-commit.sample": "39cb268e2a85d436b9eb6f47614c3cbc",
".git/hooks/pre-applypatch.sample": "054f9ffb8bfe04a599751cc757226dda",
".git/hooks/pre-push.sample": "2c642152299a94e05ea26eae11993b13",
".git/hooks/update.sample": "647ae13c682f7827c22f5fc08a03674e",
".git/hooks/push-to-checkout.sample": "c7ab00c7784efeadad3ae9b228d4b4db",
".git/refs/heads/hoa/form_booking": "31cd9b9074a93b06e9f7a86f72d29eee",
".git/refs/heads/dev": "31cd9b9074a93b06e9f7a86f72d29eee",
".git/refs/heads/main": "a8fe4757337088b6d6037bba03a1dd05",
".git/refs/remotes/origin/HEAD": "98b16e0b650190870f1b40bc8f4aec4e",
".git/refs/remotes/origin/hoa/form_booking": "c807babb53fd605abe4fefbc42a1fab1",
".git/refs/remotes/origin/main": "a8fe4757337088b6d6037bba03a1dd05",
".git/index": "9795df2061c997115f09b3d246242e23",
".git/packed-refs": "e5b5a90fd3c7b2e7498efba630627d1f",
".git/COMMIT_EDITMSG": "cba435f973fa04bb31cb8850424fb008",
".git/FETCH_HEAD": "f973a90b02ab8989dc41a6948fc277f0",
".git/smartgit/logcache/subtrees.uuid": "197183f16d5a2963c9db20aaebeaf047",
".git/smartgit/logcache/nodes3": "d70569ece386559538494e7b39819969",
".git/smartgit/logcache/nodes.uuid": "da888d9e4d153889d0664cdca34a31fb",
"assets/AssetManifest.json": "fc9b0104c86e460ea0b071f5fb585e61",
"assets/NOTICES": "12ab339478f47a60c5313ce381b5a883",
"assets/FontManifest.json": "dc3d03800ccca4601324923c0b1d6d57",
"assets/AssetManifest.bin.json": "2132125d05c8dacee031bc9c10b69289",
"assets/packages/cupertino_icons/assets/CupertinoIcons.ttf": "e986ebe42ef785b27164c36a9abc7818",
"assets/packages/fast_rsa/web/assets/worker.js": "cb436a66695a131c0f470e15a55b86a9",
"assets/packages/fast_rsa/web/assets/wasm_exec.js": "8bccb701dbf4a238e687fa92cda9c4bb",
"assets/packages/fast_rsa/web/assets/rsa.wasm": "316c2af4f44865c7a0e6cbaddef82c7e",
"assets/packages/flutter_dropzone_web/assets/flutter_dropzone.js": "5530dc96a013849f2739b2393d1b8102",
"assets/packages/flutter_framework/lib/assets/images/backgrounds/404.jpg": "bbf9e5988e15c6fb1eb758b02a851a76",
"assets/packages/flutter_framework/lib/assets/images/backgrounds/403.jpg": "062d4726a6214c0634ccd01c20e823a0",
"assets/shaders/ink_sparkle.frag": "ecc85a2e95f5e9f53123dcaf8cb9b6ce",
"assets/lib/assets/images/backgrounds/captcha.jpeg": "d6f427d39799870d8aa0b5a424fcfd84",
"assets/lib/assets/images/icons/success.png": "f5004ec62f17423fdcdd7a84e6f91462",
"assets/lib/assets/images/icons/failure.png": "10b82051f550d8d99a218981a3680122",
"assets/lib/assets/images/icons/pending.png": "992a3ce3aff73217a374a57d2295d424",
"assets/lib/assets/resources/en.json": "24d413ccd80d765b522d65e0bc32f38e",
"assets/lib/assets/resources/vi.json": "c6fb9acee5e5347dc34cc166169dbbeb",
"assets/AssetManifest.bin": "a68ac49aafc11e84bb5544516cf0687f",
"assets/fonts/MaterialIcons-Regular.otf": "4c4cfcad2d9540e08bc7926e2a491f46",
"canvaskit/skwasm.js": "694fda5704053957c2594de355805228",
"canvaskit/skwasm.js.symbols": "262f4827a1317abb59d71d6c587a93e2",
"canvaskit/canvaskit.js.symbols": "48c83a2ce573d9692e8d970e288d75f7",
"canvaskit/skwasm.wasm": "9f0c0c02b82a910d12ce0543ec130e60",
"canvaskit/chromium/canvaskit.js.symbols": "a012ed99ccba193cf96bb2643003f6fc",
"canvaskit/chromium/canvaskit.js": "671c6b4f8fcc199dcc551c7bb125f239",
"canvaskit/chromium/canvaskit.wasm": "b1ac05b29c127d86df4bcfbf50dd902a",
"canvaskit/canvaskit.js": "66177750aff65a66cb07bb44b8c6422b",
"canvaskit/canvaskit.wasm": "1f237a213d7370cf95f443d896176460",
"canvaskit/skwasm.worker.js": "89990e8c92bcb123999aa81f7e203b1c"};
// The application shell files that are downloaded before a service worker can
// start.
const CORE = ["main.dart.js",
"index.html",
"flutter_bootstrap.js",
"assets/AssetManifest.bin.json",
"assets/FontManifest.json"];

// During install, the TEMP cache is populated with the application shell files.
self.addEventListener("install", (event) => {
  self.skipWaiting();
  return event.waitUntil(
    caches.open(TEMP).then((cache) => {
      return cache.addAll(
        CORE.map((value) => new Request(value, {'cache': 'reload'})));
    })
  );
});
// During activate, the cache is populated with the temp files downloaded in
// install. If this service worker is upgrading from one with a saved
// MANIFEST, then use this to retain unchanged resource files.
self.addEventListener("activate", function(event) {
  return event.waitUntil(async function() {
    try {
      var contentCache = await caches.open(CACHE_NAME);
      var tempCache = await caches.open(TEMP);
      var manifestCache = await caches.open(MANIFEST);
      var manifest = await manifestCache.match('manifest');
      // When there is no prior manifest, clear the entire cache.
      if (!manifest) {
        await caches.delete(CACHE_NAME);
        contentCache = await caches.open(CACHE_NAME);
        for (var request of await tempCache.keys()) {
          var response = await tempCache.match(request);
          await contentCache.put(request, response);
        }
        await caches.delete(TEMP);
        // Save the manifest to make future upgrades efficient.
        await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
        // Claim client to enable caching on first launch
        self.clients.claim();
        return;
      }
      var oldManifest = await manifest.json();
      var origin = self.location.origin;
      for (var request of await contentCache.keys()) {
        var key = request.url.substring(origin.length + 1);
        if (key == "") {
          key = "/";
        }
        // If a resource from the old manifest is not in the new cache, or if
        // the MD5 sum has changed, delete it. Otherwise the resource is left
        // in the cache and can be reused by the new service worker.
        if (!RESOURCES[key] || RESOURCES[key] != oldManifest[key]) {
          await contentCache.delete(request);
        }
      }
      // Populate the cache with the app shell TEMP files, potentially overwriting
      // cache files preserved above.
      for (var request of await tempCache.keys()) {
        var response = await tempCache.match(request);
        await contentCache.put(request, response);
      }
      await caches.delete(TEMP);
      // Save the manifest to make future upgrades efficient.
      await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
      // Claim client to enable caching on first launch
      self.clients.claim();
      return;
    } catch (err) {
      // On an unhandled exception the state of the cache cannot be guaranteed.
      console.error('Failed to upgrade service worker: ' + err);
      await caches.delete(CACHE_NAME);
      await caches.delete(TEMP);
      await caches.delete(MANIFEST);
    }
  }());
});
// The fetch handler redirects requests for RESOURCE files to the service
// worker cache.
self.addEventListener("fetch", (event) => {
  if (event.request.method !== 'GET') {
    return;
  }
  var origin = self.location.origin;
  var key = event.request.url.substring(origin.length + 1);
  // Redirect URLs to the index.html
  if (key.indexOf('?v=') != -1) {
    key = key.split('?v=')[0];
  }
  if (event.request.url == origin || event.request.url.startsWith(origin + '/#') || key == '') {
    key = '/';
  }
  // If the URL is not the RESOURCE list then return to signal that the
  // browser should take over.
  if (!RESOURCES[key]) {
    return;
  }
  // If the URL is the index.html, perform an online-first request.
  if (key == '/') {
    return onlineFirst(event);
  }
  event.respondWith(caches.open(CACHE_NAME)
    .then((cache) =>  {
      return cache.match(event.request).then((response) => {
        // Either respond with the cached resource, or perform a fetch and
        // lazily populate the cache only if the resource was successfully fetched.
        return response || fetch(event.request).then((response) => {
          if (response && Boolean(response.ok)) {
            cache.put(event.request, response.clone());
          }
          return response;
        });
      })
    })
  );
});
self.addEventListener('message', (event) => {
  // SkipWaiting can be used to immediately activate a waiting service worker.
  // This will also require a page refresh triggered by the main worker.
  if (event.data === 'skipWaiting') {
    self.skipWaiting();
    return;
  }
  if (event.data === 'downloadOffline') {
    downloadOffline();
    return;
  }
});
// Download offline will check the RESOURCES for all files not in the cache
// and populate them.
async function downloadOffline() {
  var resources = [];
  var contentCache = await caches.open(CACHE_NAME);
  var currentContent = {};
  for (var request of await contentCache.keys()) {
    var key = request.url.substring(origin.length + 1);
    if (key == "") {
      key = "/";
    }
    currentContent[key] = true;
  }
  for (var resourceKey of Object.keys(RESOURCES)) {
    if (!currentContent[resourceKey]) {
      resources.push(resourceKey);
    }
  }
  return contentCache.addAll(resources);
}
// Attempt to download the resource online before falling back to
// the offline cache.
function onlineFirst(event) {
  return event.respondWith(
    fetch(event.request).then((response) => {
      return caches.open(CACHE_NAME).then((cache) => {
        cache.put(event.request, response.clone());
        return response;
      });
    }).catch((error) => {
      return caches.open(CACHE_NAME).then((cache) => {
        return cache.match(event.request).then((response) => {
          if (response != null) {
            return response;
          }
          throw error;
        });
      });
    })
  );
}
